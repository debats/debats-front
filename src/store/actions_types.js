export default {
  ENTITY_ACCESS: 'ENTITY_ACCESS',
  ENTITY_READ: 'ENTITY_READ',
  ADD_STATEMENT_VALIDATE: 'ADD_STATEMENT_VALIDATE',
  STATEMENT_POST_BEGIN: 'STATEMENT_POST_BEGIN',
};
